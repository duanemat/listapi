<?php

/** ElasticSearch implementation of lists */

namespace App\Data;

use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\Missing404Exception;

class ElasticList extends BaseList{

    private $client;
    private $list_id;

    /**
     * ElasticList constructor.
     */
    function __construct()
    {
        $this->client = ClientBuilder::create()
            ->setHosts(["localhost:9200", "192.241.134.126:9200"])
            ->build();
    }

    /**
     * Constructor with a list id
     * @param $list_id
     */
    function withId($list_id)
    {
        $this->list_id = $list_id;
    }

    /**
     * @param $list_id
     * @return mixed
     */
    protected function getList($list_id)
    {
        // TODO: Implement getList() method.
    }

    /**
     * @return mixed
     */
    protected function getListAsArray()
    {
        // TODO: Implement getListAsArray() method.
    }

    /**
     * @return mixed
     */
    protected function getListAsJson()
    {
        // TODO: Implement getListAsJson() method.
    }

    /**
     * @param $list_id
     * @param $value
     * @return mixed
     */
    protected function setList($list_id, $value)
    {
        // TODO: Implement setList() method.
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function addList($value)
    {
        // TODO: Implement addList() method.
    }


}

?>
