<?php
/**
 * Created by PhpStorm.
 * User: Matthew
 * Date: 9/18/2016
 * Time: 9:31 PM
 */

namespace App\Data;

use Log;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Prophecy\Exception\Exception;

class ElasticNote extends BaseNote
{

    private $client = null;
    private $note_id, $note_index, $note_type;

    private $note_data = array(
        "id" => null,
        "index" => null,
        "type" => null,
        "title" => null,
        "text" => null,
        "creation_timestamp" => null,
        "modified_timestamp" => null,
        "deleted_timestamp" => null,
        "deleted" => null
    );

    // User key=>Storage key
    private $note_keys_arr = [
        "id" => "id",
        "index" => "index",
        "type" => "type",
        "title" => "title",
        "text" => "text",
        "deleted" => "deleted",
        "creation_timestamp" => "creation_timestamp",
        "modified_timestamp" => "modified_timestamp",
        "deleted_timestamp" => "deleted_timestamp"
    ];

    private function createConnection()
    {
        $this->client = ClientBuilder::create()
            ->setHosts(config('elasticsearch.hosts'))
            ->build();
    }

    /**
     * ElasticNote constructor.
     */
    function __construct()
    {
        $this->createConnection();
        $this->note_data['index'] = config('elasticsearch.note.name');
        $this->note_data['type'] = config('elasticsearch.note.type');
    }

    /**
     * Constructor with a list id
     * @param $note_id
     */
    public static function withId($note_id, $note_index=null, $note_type=null)
    {
        $instance = new self();
        $instance->note_data['id'] = $note_id;
        $instance->note_id = $note_id;

        if ($note_index != null)
            $instance->note_data['index'] = $note_index;
        if ($note_type != null)
            $instance->note_data['type'] = $note_type;

        if ($instance->client == null) {
            $instance->createConnection();
        }

        $instance->getNote();
        return $instance;

    }

    /**
     * @param $note_data
     */
    public static function withData($note_data)
    {
        $instance = new self();
        $instance->extractNoteParameters($note_data);

        return $instance;
    }

    /**
     * Saves the current note
     */
    public function save()
    {

        $body = [];
        foreach($this->note_data as $key=>$val){
            $body[$key] = $val;
        }

        Log::info(print_r($body, true));
        $params = [
            'index' => $this->note_data['index'],
            'type' => $this->note_data['type'],
            'body' => $body
        ];
        if ($this->note_data['id'] != null){
            $params['id'] = $this->note_data['id'];
        }

        $response = $this->client->index($params);
        $this->note_data['id'] = $response['id'];

        return $response;
    }

    /**
     * @return True if deleted, false if not
     */
    public function delete()
    {
        Log::info("Deleting note with id ".$this->note_id);
        try{
            $params = [
                'index'=>$this->note_data['index'],
                'type'=>$this->note_data['type'],
                'id'=>$this->note_id,
                'body'=>[
                    'doc'=>[
                        'deleted'=>true,
                        'deleted_timestamp'=>time()
                    ]
                ]
            ];
            $this->client->update($params);
        }catch (Exception $e){
            Log::error("Error deleting the note $this->note_id because: ".$e->getMessage());
            return false;
        }
        Log::info("Deleted note $this->note_id successfully");
        $this->note_data = null;
        $this->note_id=null;
        return true;
    }

    /**
     * @param $note_id
     * @return mixed
     */
    public function getNote()
    {
        // TODO: Implement getNote() method.
        $params = [
            'index' => $this->note_data['index'],
            'type' => $this->note_data['type'],
            'id' => $this->note_data['id']
        ];
        try {
            $response = $this->client->get($params);
        } catch (Missing404Exception $e) {
            Log::error($e->getMessage());
            return null;
        }

        $response = $response['_source'];
        foreach ($this->note_keys_arr as $key => $val) {
            if (isset($response[$val]) || array_key_exists($val, $response)) {
                $this->note_data[$key] = $response[$val];
            }
        }

        Log::info(sprintf("%s", print_r($response, true)));
        return $this;
    }

    public function getValue($key)
    {
        if (array_key_exists($key, $this->note_keys_arr)) {
            return $this->note_data[$key];
        }
    }

    /**
     *
     */
    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getNoteAsJson();
    }

    private function extractNoteParameters($note_data)
    {
        foreach($this->note_data as $key=>$val){
            try {
                if (isset($note_data[$key]))
                    $this->note_data[$key] = $note_data[$key];
            }catch(\ErrorException $e){
                echo $e->getMessage();
            }
        }
    }

    /**
     * @return mixed
     */
    protected function getNoteAsJson()
    {
        // TODO: Implement getNoteAsJson() method.
        try {
            return json_encode($this->note_data);
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    protected function getNoteAsArray()
    {
        // TODO: Implement getNoteAsArray() method.
    }



    /**
     * Creates the index and mapping for a note
     */
    public function createNoteIndex()
    {
        $params = [
            'index' => 'note',
            'body' => [
                'settings' => [
                    'number_of_shards' => 5,
                    'number_of_replicas' => 0,
                    'analysis' => [
                        'filter' => [
                            'shingle'=> [
                                'type' => 'shingle'
                            ]
                        ],
                        'analyzer' => [
                            'note_analyzer' => [
                                'type' => 'custom',
                                'tokenizer' => 'standard',
                                'filter' => [
                                    'lowercase',
                                    'kstem'
                                ]
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    'note' => [
                        'properties' => [
                            'title' => [
                                'type' => 'string',
                                'analyzer' => 'note_analyzer',
                            ],
                            'text' => [
                                'type' => 'string',
                                'analyzer' => 'note_analyzer',
                            ],
                            'creation_timestamp' => [
                                'type' => 'date'
                            ],
                            'modified_timestamp' => [
                                'type' => 'date'
                            ],
                            'deleted_timestamp' => [
                                'type' => 'date'
                            ],
                            'deleted' => [
                                'type' => 'boolean'
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $this->client->indices()->create($params);
    }
}
