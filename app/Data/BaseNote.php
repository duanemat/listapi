<?php
/**
 * Base Class for all Notes
 * User: Matthew
 * Date: 9/18/2016
 * Time: 9:29 PM
 */

namespace App\Data;

abstract class BaseNote
{
    abstract protected function getNote();
    abstract protected function getNoteAsArray();
    abstract protected function getNoteAsJson();
    abstract protected function save();
    abstract protected function delete();

    public function printList(){
        return $this->getNoteAsJson()."\n";
    }
}
