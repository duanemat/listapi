<?php
/** Base Class for all List implementations */

namespace App\Data;


abstract class BaseList
{
    abstract protected function getList($list_id);
    abstract protected function getListAsArray();
    abstract protected function getListAsJson();
    abstract protected function setList($list_id, $value);
    abstract protected function addList($value);

    public function printList(){
        return $this->getListAsJson()."\n";
    }
}