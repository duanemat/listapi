<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Response;
use Elasticsearch\ClientBuilder;
use Exception;
use Elasticsearch\Common\Exceptions\Missing404Exception;

use App\Data\ElasticList;

class Controller extends BaseController
{
    public function getHelloWorld($id){
        $simple = new ElasticList();
        $client = ClientBuilder::create()
          ->setHosts(["localhost:9200", "192.241.134.126:9200"])
          ->build();
        $params = [
            'index' => 'test',
            'type' => 'test',
            'id' => $id
        ];
        try{
            $response = $client->get($params);
        } catch(Missing404Exception $e){
            //echo $e->getMessage();
            return response()->json(['response' => null])->setStatusCode('404');
        }
        return response()->json(['response' => $response])->setStatusCode('200');
    }
}
