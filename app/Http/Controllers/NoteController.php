<?php
/**
 * Created by PhpStorm.
 * User: Matthew
 * Date: 9/18/2016
 * Time: 10:19 PM
 */

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Exception;
use Log;
use Elasticsearch\Common\Exceptions\Missing404Exception;

use App\Data\ElasticNote;

class NoteController extends BaseController
{
    public function getNote($note_id)
    {
        $note = ElasticNote::withId($note_id);
        if ($note != null)
            return (new Response($note->getNote(), 200))
                ->header('Content-Type', 'application/json');
        else{
            return (new Response(null, 404));
        }
    }

    public function createNote(Request $request)
    {
        $note = new ElasticNote();
        $note->createNoteIndex();
        $data = $request->toArray();
        $data['index'] = config('elasticsearch.note.name');
        $data['creation_timestamp'] = time();
        $data['modified_timestamp'] = $data['creation_timestamp'];
        if (!isset($data['type'])) {
            $data['type'] = config('elasticsearch.note.type');
        }
        $note = ElasticNote::withData($data);

        return $note->save();

    }

    public function deleteNote($note_id){
        Log::info($note_id);
        $note = ElasticNote::withId($note_id);
        if ($note != null){
            $note_deleted = $note->delete();
            return (new Response(($note_deleted == true ? "Deleted" : "Not deleted"), 200))
                    ->header('Content-Type', 'application/json');
        }
        else{
            return (new Response(null, 404));
        }
    }

}
