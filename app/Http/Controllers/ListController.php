<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Response;
use Elasticsearch\ClientBuilder;
use Exception;
use Elasticsearch\Common\Exceptions\Missing404Exception;

use App\Data\ElasticList;

class ListController extends BaseController
{
    private $ES_HOST;

    function __construct(){
        //$this->ES_HOST = Array(getenv('ES_HOST'));
        $this->ES_HOST = config('elasticsearch.hosts');
    }
    public function getList($id){
        $simple = new ElasticList();
        $client = ClientBuilder::create()
          ->setHosts($this->ES_HOST)
          ->build();
        $params = [
            'index' => 'test',
            'type' => 'test',
            'id' => $id
        ];
        try{
            $response = $client->get($params);
        } catch(Missing404Exception $e){
            //echo $e->getMessage();
            return response()->json(['response' => null])->setStatusCode('404');
        }
        return response()->json(['response' => $response])->setStatusCode('200');
    }
}
