<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/es', function() use ($app){
    $es_client = new \Elasticsearch\Client();
    return print_r($es_client, true);
});

$app->get('/test/{id}', 'ListController@getList');
$app->post('/test', 'ListController@setList');

$app->get('/note/{note_id}', 'NoteController@getNote');
$app->post('/note/', 'NoteController@createNote');
$app->delete('/note/{note_id}', 'NoteController@deleteNote');

